import React from 'react';
import ReactPlayer from "react-player"

function Player() {

	return (
		<div className="mx-auto" style={{"width": "800px"}}>
			<ReactPlayer url="http://take-home-assignment.tempo.fit/videos/python-data.mp4" controls width='100%' height='100%'/>
		</div>
	)
}

export default Player;
